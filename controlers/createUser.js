import postUsers from "./postUser.js";

const botao = document.querySelector("#botao");
const inputNome = document.querySelector("#nome");
const inputEmail = document.querySelector("#email");
const inputSenha = document.querySelector("#senha");
const inputDataNasc = document.querySelector("#dataNasc");
const erroNome = document.querySelector(".erro");


botao.addEventListener("click", async (event) => {
  event.preventDefault();
  const nome = inputNome.value;
  const email = inputEmail.value;
  const senha = inputSenha.value;
  const dataNasc = inputDataNasc.value;
  

if (nome === "" || email === "" || senha ==="" || dataNasc==="") {

    erroNome.classList.remove("invisivel");

  } else {

    erroNome.classList.add("invisivel");
    await postUsers(nome, email,senha,dataNasc);

  }

});
