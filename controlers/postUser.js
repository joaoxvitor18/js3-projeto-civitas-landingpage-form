const postUsers = async (nome, email,senha,dataNasc) => {
    try {
      const resposta = await fetch("http://localhost:3000/users", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          nome: `${nome}`,
          email: `${email}`,
          senha: `${senha}`,
          dataNasc: `${dataNasc}`,
        }),
      });
      const dataUser = await resposta.json();
  
      return dataUser;
    } catch (error) {
      console.log(error);
    }
  };
  
  export default postUsers;
  