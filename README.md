# PROJETO JS-III

## Conteúdo do projeto

- JavaScript - DOM
- HTTP protocol
- CRUD - POST method
- Fetch API

<hr><br>

## Tecnologias utilizadas

- [JavaScript](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript)
- [Fetch API](https://developer.mozilla.org/pt-BR/docs/Web/API/Fetch_API)
- [JSON Server](https://www.npmjs.com/package/json-server)

<hr><br>

## Como executar o projeto
<br>

#### Clone o repositório

```
git clone https://gitlab.com/joaoxvitor18/js3-projeto-civitas-landingpage-form.git
```

#### Acesse o diretório do projeto

```
cd LandingPage
```

#### Instale o NPM no terminal para baixar os node_modules via:

```
npm install
```

#### Inicie a API via terminal pelo comando:

```
npx json-server --watch db.json
```

#### Abra o arquivo `index.html` para iniciar 

### Preencha o formulário ao final da página e clique em "Registre-se". Esses dados já servirão à API

<br>

#### Execute o comando `CTRL + C` no terminal para fechar a API

#### Projeto baseado no projeto de Vitor Giórgio.

<hr>

<div align="center">
MIT License<br>
Copyright © 2022<br> 
Criado por <b>João Assumpção</b>
</div>
