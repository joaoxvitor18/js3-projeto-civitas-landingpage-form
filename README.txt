Trainee: João Assumpção
Data: 28/03

Para utilizar a API via json-server utlizada neste projeto, é necessário:

- Clonar este repositório
- Instalar o NPM para baixar os node_modules via npm -install
- Iniciar a API via "npx json-server --watch db.json" no terminal
- Abrir o arquivo .html "index.html"
- Preencher os dados no formulário de registro ao final da página e clicar em "Registre-se", os dados então serão enviados para a API via método POST
- Para encerrar a API, basta executar Ctrl+C no terminal



